
import 'package:google_maps_flutter/google_maps_flutter.dart' as google;
import 'package:maps_toolkit/maps_toolkit.dart';

google.Polygon detectArea(google.LatLng latLng , List<google.Polygon> polygons){
  for(int i=0 ; i< polygons.length ; i++){
    if(PolygonUtil.containsLocation(LatLng(latLng.latitude, latLng.longitude), getPolygonPoints(polygons[i]), true))
      return polygons[i];
  }

  return null;
}


List<LatLng> getPolygonPoints(google.Polygon polygon){
  return polygon.points.map((e) => LatLng(e.latitude, e.longitude)).toList();
}